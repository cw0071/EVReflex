#!/usr/bin/env python3
import torch
import platform
from pathlib import Path
from git import Repo
import inspect
import time
import setproctitle
import numpy as np
import cv2

eps = 1e-12

# ==================================================================================================
def get_system_info():
	return {
		"hostname": platform.uname()[1],
		"gpus": get_gpu_info(),
	}


# ==================================================================================================
def get_gpu_info():
	gpus = []
	for gpu in range(torch.cuda.device_count()):
		properties = torch.cuda.get_device_properties(f"cuda:{gpu}")
		gpus.append({
			"name": properties.name,
			"memory": round(properties.total_memory / 1e9, 2),
			"capability": f"{properties.major}.{properties.minor}",
		})

	return gpus


#===================================================================================================
def get_git_rev(cwd=Path(inspect.stack()[1][1]).parent): # Parent of called script by default
	repo = Repo(cwd)
	sha = repo.head.commit.hexsha
	output = repo.git.rev_parse(sha, short=7)
	if repo.is_dirty():
		output += " (dirty)"
	output += " - " + time.strftime("%a %d/%m/%Y %H:%M", time.gmtime(repo.head.commit.committed_date))

	return output


# ==================================================================================================
def proctitle(title: str = None):
	import __main__
	if title:
		setproctitle.setproctitle(title)
	else:
		setproctitle.setproctitle(Path(__main__.__file__).name)


# ==================================================================================================
def flow_viz(tensor: torch.Tensor) -> torch.Tensor:
	assert tensor.dim() == 4
	assert tensor.shape[1] == 2
	flowImg = torch.zeros([tensor.shape[0], 3, tensor.shape[2], tensor.shape[3]], dtype=torch.uint8)
	for i, f in enumerate(flowImg):
		flowImg[i] = torch.tensor(_flow_viz_np(tensor[i, 0, ...].numpy(), tensor[i, 1, ...].numpy())).permute(2, 0, 1)

	return flowImg


# ==================================================================================================
def _flow_viz_np(flow_x, flow_y):
	flows = np.stack((flow_x, flow_y), axis=2)
	flows[np.isinf(flows)] = 0
	flows[np.isnan(flows)] = 0
	mag = np.linalg.norm(flows, axis=2)
	ang = np.arctan2(flow_y, flow_x)
	p = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
	ang += np.pi
	ang *= 180. / np.pi / 2.
	ang = ang.astype(np.uint8)
	hsv = np.zeros([flow_x.shape[0], flow_x.shape[1], 3], dtype=np.uint8)
	hsv[:, :, 0] = ang
	hsv[:, :, 1] = 255
	hsv[:, :, 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
	flow_rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

	return flow_rgb


# ==================================================================================================
def downsample_image_tensor(tensor):
	assert(tensor.dim() >= 1)
	assert(tensor.shape[-1] % 2 == 0 and tensor.shape[-2] % 2 == 0)
	down = tensor[..., ::2, ::2]
	up = down.repeat_interleave(2, -2).repeat_interleave(2, -1)

	return up


# ==================================================================================================
def calc_iou(tensor, target, mask=None):
	intersection = (tensor & target).float() # AND
	union = (tensor | target).float() # OR
	# Subtract ignored pixels from both intersection and union
	if mask is not None:
		intersection[mask] = 0
		union[mask] = 0
	iou = (intersection.sum((-2, -1)) + eps) / (union.sum((-2, -1)) + eps)

	return iou
