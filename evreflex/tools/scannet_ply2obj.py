import bpy
from pathlib import Path
import numpy as np
from kellog import info, warning, error, debug
from tqdm import tqdm
from natsort import natsorted
import numpy_indexed as npi
import subprocess
import tempfile
import os
import cv2
import math
from io_scene_obj import import_obj

categories = np.array([
	(0, 0, 0), # IGNORE
	(174, 199, 232), # wall
	(152, 223, 138), # floor
	(31, 119, 180), # cabinet
	(255, 187, 120), # bed
	(188, 189, 34), # chair
	(140, 86, 75), # sofa
	(255, 152, 150), # table
	(214, 39, 40), # door
	(197, 176, 213), # window
	(148, 103, 189), # bookshelf
	(196, 156, 148), # picture
	(23, 190, 207), # counter
	(178, 76, 76), # blinds
	(247, 182, 210), # desk
	(66, 188, 102), # shelves
	(219, 219, 141), # curtain
	(140, 57, 197), # dresser
	(202, 185, 52), # pillow
	(51, 176, 203), # mirror
	(200, 54, 131), # floor mat
	(92, 193, 61), # clothes
	(78, 71, 183), # ceiling
	(172, 114, 82), # books
	(255, 127, 14), # fridge
	(91, 163, 138), # tv
	(153, 98, 156), # paper
	(140, 153, 101), # towel
	(158, 218, 229), # shower curtain
	(100, 125, 154), # box
	(178, 127, 135), # white board
	(120, 185, 128), # person
	(146, 111, 194), # night stand
	(44, 160, 44), # toilet
	(112, 128, 144), # sink
	(96, 207, 209), # lamp
	(227, 119, 194), # bathtub
	(213, 92, 176), # bag
	(94, 106, 211), # other struct
	(82, 84, 163), # other furniture
	(100, 85, 144), # other prop
], dtype=np.uint8)
# Because OpenCV does BGR down there
categories = np.array([[c[2], c[1], c[0]] for c in categories], dtype=np.uint8)


# ==================================================================================================
def iterate(path: Path):
	# ("*/") matches files too
	scenes = [i for i in natsorted(path.glob("scene*")) if i.is_dir()]
	if not len(scenes):
		raise RuntimeError("No directories found starting with 'scene'")
	for scene in tqdm(scenes):
		if not scene.is_dir():
			continue
		try:
			process(scene)
		except Exception as e:
			error(e)
			res = 8192
			warning(f"Retrying scene with a higher resolution texture ({res})")
			try:
				process(scene, res)
			except Exception as e:
				error(e)
				res = 16384
				warning(f"Retrying scene with a higher resolution texture ({res})")
				process(scene, res)
		# break
	debug("DONE")


# ==================================================================================================
def process(scene, resolution: int = 4096):
	debug(scene)
	clear_scene()
	rgbPath = scene / f"{scene.name}_vh_clean_2.ply"
	labelsPath = scene / f"{scene.name}_vh_clean_2.labels.ply"
	if not rgbPath.exists() or not labelsPath.exists():
		if not rgbPath.exists():
			raise RuntimeError(f"Cannot find '{rgbPath}'")
		if not labelsPath.exists():
			raise RuntimeError(f"Cannot find '{labelsPath}'")

	map_uvs(labelsPath, rgbPath, size=resolution)
	os.remove(labelsPath.with_suffix(".obj"))
	os.remove(f'{labelsPath.with_suffix(".obj")}.mtl')
	model = import_model(rgbPath.with_suffix(".obj"))
	centre_object_xy(model[1])
	bpy.ops.object.origin_set(type="ORIGIN_CENTER_OF_MASS")
	correct_texture(model[1], labelsPath.with_suffix(".png"))
	merge_textures(labelsPath.with_suffix(".png"), rgbPath.with_suffix(".png"))
	os.remove(labelsPath.with_suffix(".png"))

	debug("\tExporting centred mesh")
	bpy.ops.export_scene.obj(
		filepath=str(rgbPath.with_suffix(".obj")),
		check_existing=False,
		axis_forward="Y",
		axis_up="Z",
		use_triangles=True,
		keep_vertex_order=True,
		global_scale=1.0,
		path_mode="COPY"
	)


# ==================================================================================================
def clear_scene():
	debug("\tClearing scene")
	bpy.ops.object.select_all(action="DESELECT")
	# Blender version >= 2.8x?
	for obj in bpy.context.scene.objects:
		if obj.type == "MESH":
			obj.select_set(True)
		else:
			obj.select_set(False)
	bpy.ops.object.delete()
	clean_deleted()


# ==================================================================================================
def clean_deleted():
	# Remove unused blocks without closing and reopening blender
	for block in bpy.data.meshes:
		if block.users == 0:
			bpy.data.meshes.remove(block)
	for block in bpy.data.materials:
		if block.users == 0:
			bpy.data.materials.remove(block)
	for block in bpy.data.textures:
		if block.users == 0:
			bpy.data.textures.remove(block)
	for block in bpy.data.images:
		if block.users == 0:
			bpy.data.images.remove(block)


# ==================================================================================================
def map_uvs(labelsPath: Path, rgbPath: Path, size: int = 4096) -> Path:
	debug("\tMapping UVs")
	with tempfile.NamedTemporaryFile(mode="w+") as tmp:
		tmp.write(generate_script(labelsPath.with_suffix(".png"), size=size))
		tmp.seek(0)
		# Generate RGB texture AND export the mesh as a .obj file
		with subprocess.Popen(["meshlabserver", "-i", labelsPath, "-o", labelsPath.with_suffix(".obj"), "-m", "wt", "-s", tmp.name]) as ps:
			pass
	with tempfile.NamedTemporaryFile(mode="w+") as tmp:
		tmp.write(generate_script(rgbPath.with_suffix(".png"), size=size))
		tmp.seek(0)
		# Generate RGB texture
		with subprocess.Popen(["meshlabserver", "-i", rgbPath, "-o", rgbPath.with_suffix(".obj"), "-m", "wt", "-s", tmp.name]) as ps:
			pass


# ==================================================================================================
def import_model(path: Path):
	debug(f"\tImporting '{path}'")
	# bpy.ops.import_mesh.obj(filepath=str(path))
	context = bpy.context
	import_obj.load(context, filepath=str(path))

	return get_model()


# ==================================================================================================
def get_model():
	scene = bpy.data.scenes["Scene"]
	objects = scene.objects.items()
	models = [o for o in objects if o[0].startswith("scene")]
	assert(len(models) == 1)

	return models[0]


# ==================================================================================================
def vertices_equal(a: bpy.types.Mesh, b: bpy.types.Mesh) -> bool:
	"""Equal `Mesh.polygons.vertices` using __eq__ operator return False, so this function does it manually."""
	return all([c == l for ap, bp in zip(a.polygons, b.polygons) for c, l in zip(ap.vertices, bp.vertices)])


# ==================================================================================================
def verify_models(colour_model, label_model):
	debug("\tChecking for equal number of polygons...")
	if len(colour_model.polygons) != len(label_model.polygons):
		raise ValueError(f"Number of polygons do not match: {len(colour_model.polygons)} RGB vs. {len(label_model.polygons)} label")

	debug("\tChecking for equal vertices...")
	if not vertices_equal(colour_model, label_model):
		raise ValueError(f"Vertices do not match!")


# ==================================================================================================
def centre_object_xy(model):
	debug("\tCentering mesh on XY")
	bpy.context.view_layer.objects.active = model
	bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME')
	bpy.context.object.location.x = 0.0
	bpy.context.object.location.y = 0.0
	bpy.ops.object.transform_apply()


# ==================================================================================================
def correct_texture(model, labelsPath):
	debug("\tCorrecting texture interpolation")
	me = model.data
	assert(len(me.loops) % 3 == 0)
	img = cv2.imread(str(labelsPath))

	bg = np.zeros_like(img)
	# for i in tqdm(range(0, len(me.loops), 3)):
	for i in range(0, len(me.loops), 3):
		t1 = me.uv_layers.active.data[i].uv
		t2 = me.uv_layers.active.data[i + 1].uv
		t3 = me.uv_layers.active.data[i + 2].uv

		# `round()` seems to do the best job
		x1 = math.floor(img.shape[1] * t1.x)
		x2 = math.floor(img.shape[1] * t2.x)
		x3 = math.floor(img.shape[1] * t3.x)
		y1 = img.shape[0] - round(img.shape[0] * t1.y)
		y2 = img.shape[0] - round(img.shape[0] * t2.y)
		y3 = img.shape[0] - round(img.shape[0] * t3.y)
		c1 = img[y1, x1]
		c2 = img[y2, x2]
		c3 = img[y3, x3]

		if (c1 == c2).all() and (c1 == c2).all() and (c1 == c3).all():
			c = c1.tolist()
		if (c1 == c2).all() or (c1 == c3).all():
			c = c1.tolist()
		elif (c2 == c3).all():
			c = c2.tolist()
		else:
			c = c1.tolist()

		contours = np.array([[x1, y1], [x2, y2], [x3, y3]]).astype(int)
		cv2.drawContours(bg, [contours], 0, c, thickness=2)
		cv2.fillPoly(bg, pts=[contours], color=c)
	cv2.imwrite(str(labelsPath), bg)


# ==================================================================================================
def merge_textures(labelPath, rgbPath):
	debug("\tMerging textures")
	labels = cv2.imread(str(labelPath))
	rgb = cv2.imread(str(rgbPath))

	labels_ = labels.reshape([labels.shape[0] * labels.shape[1], 3])

	indices = npi.indices(categories, labels_, missing=0)
	# debug(np.unique(indices))
	labels__ = indices.reshape([*labels.shape[:2], 1]).astype(np.uint8)
	img = np.dstack([rgb, labels__])
	cv2.imwrite(str(rgbPath), img)


# ==================================================================================================
class OT_TestOpenFilebrowser(bpy.types.Operator):
	bl_idname = "test.open_filebrowser"
	bl_label = "Choose"

	# Instead of inheriting from ImportHelper
	directory: bpy.props.StringProperty(
		name="Path",
		description="Path to directory"
	)
	# ----------------------------------------------------------------------------------------------
	def invoke(self, context, event):
		"""Called when registering the class."""
		context.window_manager.fileselect_add(self)

		return {"RUNNING_MODAL"}

	# ----------------------------------------------------------------------------------------------
	def execute(self, context):
		"""Called when the file browser is submitted"""
		iterate(Path(self.directory))

		bpy.utils.unregister_class(self.__class__)

		return {"FINISHED"}


# ==================================================================================================
if __name__ == "__main__":
	print("--------------------------------")
	bpy.context.scene.render.engine = "CYCLES"
	bpy.utils.register_class(OT_TestOpenFilebrowser)
	bpy.ops.test.open_filebrowser("INVOKE_DEFAULT")


# ==================================================================================================
def generate_script(path: Path, size: int = 4096) -> str:
	return f"""\
<!DOCTYPE FilterScript>
<FilterScript>
 <filter name="Parametrization: Trivial Per-Triangle">
  <Param tooltip="Indicates how many triangles have to be put on each line (every quad contains two triangles)&#xa;Leave 0 for automatic calculation" isxmlparam="0" description="Quads per line" value="0" name="sidedim" type="RichInt"/>
  <Param tooltip="Gives an indication on how big the texture is" isxmlparam="0" description="Texture Dimension (px)" value="{size}" name="textdim" type="RichInt"/>
  <Param tooltip="Specifies how many pixels to be left between triangles in parametrization domain" isxmlparam="0" description="Inter-Triangle border (px)" value="4" name="border" type="RichInt"/>
  <Param tooltip="Choose space optimizing to map smaller faces into smaller triangles in parametrizazion domain" enum_cardinality="2" isxmlparam="0" description="Method" enum_val1="Space-optimizing" value="0" name="method" enum_val0="Basic" type="RichEnum"/>
 </filter>
 <filter name="Transfer: Vertex Color to Texture">
  <Param name="textName" type="RichString" tooltip="The texture file to be created" value="{path.name}" description="Texture file" isxmlparam="0"/>
  <Param name="textW" type="RichInt" tooltip="The texture width" value="{size}" description="Texture width (px)" isxmlparam="0"/>
  <Param name="textH" type="RichInt" tooltip="The texture height" value="{size}" description="Texture height (px)" isxmlparam="0"/>
  <Param name="overwrite" type="RichBool" tooltip="if current mesh has a texture will be overwritten (with provided texture dimension)" value="false" description="Overwrite texture" isxmlparam="0"/>
  <Param name="assign" type="RichBool" tooltip="assign the newly created texture" value="true" description="Assign texture" isxmlparam="0"/>
  <Param name="pullpush" type="RichBool" tooltip="if enabled the unmapped texture space is colored using a pull push filling algorithm, if false is set to black" value="true" description="Fill texture" isxmlparam="0"/>
 </filter>
</FilterScript>
"""
