#!/usr/bin/env python
import argparse
from pathlib import Path
import re
from sys import prefix
from natsort import natsorted
import lmdb
import pickle
from kellog import info, warning, error, debug
try:
	from cv_bridge import CvBridge
except ModuleNotFoundError as e:
	error(e)
	error("Probably can't find ROS, did you unset PYTHONPATH?")
	quit(1)
from tqdm import tqdm
from rosbag import Bag
import yaml
import rospy
import numpy as np
import cv2
import open3d as o3d

from typing import Tuple

# TODO do this dynamically/with argparse
# This is EVDodge's values, as used in simulator
width = 346
height = 260
fx = 172.9999924379297
fy = 172.9999924379297
cx = 173.0
cy = 130.0
labels = [i + 1 for i in range(40)] # Match the values in model conversion script...
bridge = CvBridge()
eps = 1e-15
framerate = 30 # FIX use actual image times instead
maxVelFilter = 5 # m/s to be ignored

# ==================================================================================================
def main(args):
	if args.input_path.is_file():
		outputDir = args.input_path.parent / "lmdb" / args.input_path.stem
		outputDir.mkdir(parents=True, exist_ok=True)
	elif args.input_path.is_dir():
		outputDir = args.input_path / "lmdb"
		outputDir.mkdir(parents=True, exist_ok=True)
	else:
		raise TypeError(f"Input path '{args.input_path.name}' is not a normal file or directory")
	# outputImg = outputDir / "img"
	# outputFlow = outputDir / "flow"
	# outputSeg = outputDir / "seg"
	# outputDepth = outputDir / "depth"
	# outputPCL = outputDir / "pcl"
	# outputEvent = outputDir / "event"
	outputDanger = outputDir / "danger"
	# if not args.force and any([o.exists() for o in [outputImg, outputFlow, outputSeg, outputDepth, outputEvent, outputDanger]]):
	# if not args.force and any([o.exists() for o in [outputImg, outputFlow, outputSeg, outputPCL, outputEvent, outputDanger]]):
	if not args.force and any([o.exists() for o in [outputDanger]]):
		warning(f"Output lmdb(s) exists in '{outputDir}'")
		ans = input("Overwrite? [Y/n] ").lower()
		if ans not in ["", "y"]:
			warning("Not overwriting, aborting")
			quit(0)
	info(f"Saving output lmdbs in '{outputDir}'")

	keys_img = set()
	keys_flow = set()
	keys_seg = set()
	keys_pcl = set()
	keys_event = set()
	keys_danger = set()
	# with lmdb.open(path=str(outputImg), map_size=2 ** 40) as lmdb_img, \
	# 	lmdb.open(path=str(outputFlow), map_size=2 ** 40) as lmdb_flow, \
	# 	lmdb.open(path=str(outputSeg), map_size=2 ** 40) as lmdb_seg, \
	# 	lmdb.open(path=str(outputPCL), map_size=2 ** 40) as lmdb_pcl, \
	# 	lmdb.open(path=str(outputEvent), map_size=2 ** 40) as lmdb_event, \
	# 	lmdb.open(path=str(outputDanger), map_size=2 ** 40) as lmdb_danger:
	with lmdb.open(path=str(outputDanger), map_size=2 ** 40) as lmdb_danger:
		if args.input_path.is_file():
			bagPath = args.input_path
			with Bag(str(bagPath.resolve()), "r") as bag:
				process_bag(
					bag,
					outputDir,
					# (lmdb_img, lmdb_flow, lmdb_seg, lmdb_pcl, lmdb_event, lmdb_danger),
					lmdb_danger,
					# (keys_img, keys_flow, keys_seg, keys_pcl, keys_event, keys_danger),
					keys_danger,
					args
				)
		elif args.input_path.is_dir():
			# Filter to valid bag names in case there are any extra bags in here (e.g. from debugging)
			glob = [f for f in args.input_path.rglob("*.bag") if re.search(r"scene[0-9][0-9][0-9][0-9]_[0-9][0-9]_vh_clean_2.bag$", f.name)]
			barBags = tqdm(total=len(glob), position=1)
			for bagPath in natsorted(glob):
				barBags.set_description(bagPath.name)
				with Bag(str(bagPath.resolve()), "r") as bag:
					process_bag(
						bag,
						outputDir,
						# (lmdb_img, lmdb_flow, lmdb_seg, lmdb_pcl, lmdb_event, lmdb_danger),
						# (keys_img, keys_flow, keys_seg, keys_pcl, keys_event, keys_danger),
						lmdb_danger,
						keys_danger,
						args
					)
		m = min([len(s) for s in [keys_img, keys_flow, keys_seg, keys_pcl, keys_event, keys_danger]])
		for env, keys in zip(
			# [lmdb_img, lmdb_flow, lmdb_seg, lmdb_pcl, lmdb_event, lmdb_danger],
			# [keys_img, keys_flow, keys_seg, keys_pcl, keys_event, keys_danger]
			[lmdb_danger],
			[keys_danger]
		):
			# Dirty hack to make the lists the same
			keys = natsorted(keys)
			keys = keys[1:m]
			with env.begin(write=True) as txn:
				txn.put(
					key=pickle.dumps("keys"),
					value=pickle.dumps(keys),
					dupdata=False,
				)


# ==================================================================================================
def process_bag(bag, outputDir: Path, lmdbs, keys, args):
	# lmdb_img, lmdb_flow, lmdb_seg, lmdb_pcl, lmdb_event, lmdb_danger = lmdbs
	# keys_img, keys_flow, keys_seg, keys_pcl, keys_event, keys_danger = keys
	lmdb_danger = lmdbs
	keys_danger = keys
	imgIter = 0
	eventImgIter = 0
	firstImgTime = -1
	events = []
	imgTimes = []
	eventCountImgs = []
	eventTimeImgs = []
	eventImgTimes = []
	width = None # 346
	height = None # 260

	bagInfo = yaml.load(bag._get_yaml_info(), Loader=yaml.FullLoader)
	topics = [t["topic"] for t in bagInfo["topics"] if t["topic"] in ["/cam0/image_raw", "/cam0/events", "/cam0/optic_flow", "/cam0/image_alpha", "/cam0/depthmap"]]
	tStart = bag.get_start_time()
	tStartRos = rospy.Time(tStart)

	recvd = {}
	seg = None
	depth = None
	prevDepth = None
	prevFlow = None
	flow = None
	total = sum([topic["messages"] for topic in bagInfo["topics"] if topic["topic"] in ["/cam0/image_raw", "/cam0/events", "/cam0/optic_flow", "/cam0/image_alpha", "/cam0/depthmap"]])
	# FIX total doesn't take into account start time in read_messages()
	if args.debug:
		warning("DEBUG MODE ENABLED, only doing first 0.5 seconds")
		bar = tqdm(bag.read_messages(topics=topics, end_time=rospy.Time(tStart + 0.5)), total=total)
	else:
		bar = tqdm(bag.read_messages(topics=topics), total=total)
	for topic, msg, t in bar:
		stamp = msg.header.stamp.to_sec()
		if topic != "/cam0/events":
			bar.set_description(f"{imgIter} ({msg.header.stamp.to_sec():.2f}s)")
		if topic != "/cam0/events" and stamp not in recvd:
			if len(recvd) >= 10:
				error("Problem with accumulation... timestamps:")
				for r, v in recvd.items():
					error(r, v)
				quit(1)
			recvd[stamp] = [False, False, False, False]
		if topic == "/cam0/image_raw":
			if recvd[stamp][0]:
				error("Duplicate raw!")
			else:
				recvd[stamp][0] = True
		if topic == "/cam0/image_raw":
			if not width:
				width = msg.width
			if not height:
				height = msg.height
			# img = np.asarray(bridge.imgmsg_to_cv2(msg, msg.encoding))
			# imgPath = save_img(img, imgIter, outputDir, prefix="img")
			# save_to_lmdb(lmdb_img, imgPath, keys_img, args.keep)
			time = msg.header.stamp
			if imgIter > 0:
				imgTimes.append(time)
			else:
				firstImgTime = time
				eventImgTimes.append(time.to_sec())
				# filter events we added previously
				events = filter_events(events, eventImgTimes[-1] - tStart)
		elif topic == "/cam0/optic_flow":
			if recvd[stamp][1]:
				error("Duplicate flow!")
			else:
				recvd[stamp][1] = True
			flowPath, flow = save_flow(msg, imgIter, outputDir)
			# save_to_lmdb(lmdb_flow, flowPath, keys_flow, args.keep)
		elif topic == "/cam0/image_alpha":
			if recvd[stamp][2]:
				error("Duplicate alpha!")
			else:
				recvd[stamp][2] = True
			seg = np.asarray(bridge.imgmsg_to_cv2(msg, msg.encoding))
			seg = seg * np.in1d(seg, labels).reshape(seg.shape) # Remove non-class values
			# segPath = save_img(seg, imgIter, outputDir, prefix="seg", isRGB=False)
			# save_to_lmdb(lmdb_seg, segPath, keys_seg, args.keep)
		elif topic == "/cam0/depthmap":
			if recvd[stamp][3]:
				error("Duplicate depth!")
			else:
				recvd[stamp][3] = True
			depth = np.asarray(bridge.imgmsg_to_cv2(msg, msg.encoding))
		elif topic == "/cam0/events" and msg.events:
			# Add events to list.
			for event in msg.events:
				ts = event.ts
				event = [event.x,
						event.y,
						(ts - tStartRos).to_sec(),
						(float(event.polarity) - 0.5) * 2]
				# Add event if it was after the first img or we haven't seen the first img
				if firstImgTime == -1 or ts > firstImgTime:
					events.append(event)
			# if len(imgTimes) >= args.max_aug and events[-1][2] > (imgTimes[args.max_aug - 1] - tStartRos).to_sec():
			# 	eventImgIter, keys_event = save_events(
			# 		events,
			# 		outputDir,
			# 		imgTimes,
			# 		eventCountImgs,
			# 		eventTimeImgs,
			# 		eventImgTimes,
			# 		width,
			# 		height,
			# 		args.max_aug,
			# 		args.n_skip,
			# 		eventImgIter,
			# 		tStartRos,
			# 		lmdb_event,
			# 		keys_event,
			# 		args.keep
			# 	)
					# bagPath,
		if topic != "/cam0/events" and recvd[stamp] == [True, True, True, True]:
			# pclPath = save_pcl(depth, seg, imgIter, outputDir)
			# save_to_lmdb(lmdb_pcl, pclPath, keys_pcl, args.keep)
			dangerPath = generate_danger(depth, prevDepth, imgIter, outputDir, flow, prevFlow)
			save_to_lmdb(lmdb_danger, dangerPath, keys_danger, args.keep, offset=1)
			seg = None
			prevDepth = depth
			prevFlow = flow
			depth = None
			imgIter += 1
			recvd.pop(stamp)
	if len(recvd) > 0:
		error(f"Unfinished timestamps: {len(recvd)} (out of {imgIter})")


# ==================================================================================================
def save_img(img, iter, outDir, prefix: str, isRGB: bool = True) -> Path:
	if isRGB:
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	outPath = outDir / f"{prefix}{iter:05d}.png"
	cv2.imwrite(str(outPath), img)

	return outPath


# ==================================================================================================
def save_events(events, outDir, imgTimes, eventCountImgs, eventTimeImgs,
		eventImgTimes, width, height, max_aug, n_skip, eventImgIter, tStartRos, lmdb, keys, keep) -> Tuple[int, set]:
	eventIter = 0
	cutoffEventIter = 0
	imgIter = 0
	currImgTime = (imgTimes[imgIter] - tStartRos).to_sec()

	eventCountImg = np.zeros((height, width, 2), dtype=np.uint16)
	eventTimeImg = np.zeros((height, width, 2), dtype=np.float32)

	while imgIter < len(imgTimes) and events[-1][2] > currImgTime:
		x = events[eventIter][0]
		y = events[eventIter][1]
		t = events[eventIter][2]

		if t > currImgTime:
			eventCountImgs.append(eventCountImg)
			eventCountImg = np.zeros((height, width, 2), dtype=np.uint16)
			eventTimeImgs.append(eventTimeImg)
			eventTimeImg = np.zeros((height, width, 2), dtype=np.float32)
			cutoffEventIter = eventIter
			eventImgTimes.append(imgTimes[imgIter].to_sec())
			imgIter += n_skip
			if (imgIter < len(imgTimes)):
				currImgTime = (imgTimes[imgIter] - tStartRos).to_sec()

		if events[eventIter][3] > 0:
			eventCountImg[y, x, 0] += 1
			eventTimeImg[y, x, 0] = t
		else:
			eventCountImg[y, x, 1] += 1
			eventTimeImg[y, x, 1] = t

		eventIter += 1

	del imgTimes[:imgIter]
	del events[:cutoffEventIter]

	if len(eventCountImgs) >= max_aug:
		n_to_save = len(eventCountImgs) - max_aug + 1
		for i in range(n_to_save):
			imgTimesOut = np.array(eventImgTimes[i:i + max_aug + 1])
			imgTimesOut = imgTimesOut.astype(np.float64)
			eventTimeImgsNP = np.array(eventTimeImgs[i:i + max_aug], dtype=np.float32)
			eventTimeImgsNP -= imgTimesOut[0] - tStartRos.to_sec()
			eventTimeImgsNP = np.clip(eventTimeImgsNP, a_min=0, a_max=None)

			now = np.array([np.array(eventCountImgs[i:i + max_aug]), eventTimeImgsNP, imgTimesOut], dtype=object)
			outPath = outDir / f"event{eventImgIter:05d}.npz"
			np.savez_compressed(outPath, now)
			save_to_lmdb(lmdb, outPath, keys, keep)
			eventImgIter += n_skip

		del eventCountImgs[:n_to_save]
		del eventTimeImgs[:n_to_save]
		del eventImgTimes[:n_to_save]
	return eventImgIter, keys


# ==================================================================================================
def save_flow(msg, iter, outDir):
	# It's row-major order
	flow = np.array([
		np.array(msg.flow_x).reshape([msg.height, msg.width]),
		np.array(msg.flow_y).reshape([msg.height, msg.width])
	])
	# outPath = outDir / f"flow{iter:05d}.npz"
	outPath = None
	# np.savez_compressed(str(outPath), flow.astype(np.float32))

	return outPath, flow


# ==================================================================================================
def save_pcl(depth, seg, iter, outDir: Path) -> Path:
	# Duplicating channels because o3d doesn't seem to be able to do greyscale
	depth = o3d.geometry.Image(depth)
	seg = o3d.geometry.Image(cv2.cvtColor(seg, cv2.COLOR_GRAY2RGB))
	rgbd = o3d.geometry.RGBDImage.create_from_color_and_depth(seg, depth, depth_scale=1.0, depth_trunc=50.0, convert_rgb_to_intensity=False)
	intrinsic = o3d.camera.PinholeCameraIntrinsic(width, height, fx, fy, cx, cy)
	# pcl = o3d.geometry.PointCloud.create_from_depth_image(depth, intrinsic)
	pcl = o3d.geometry.PointCloud.create_from_rgbd_image(rgbd, intrinsic, project_valid_depth_only=False)
	# File extension dictates the format
	# NOTE: `compressed` does not seem to have an effect when using 'xyzrgb'
	# o3d.io.write_point_cloud(str(path / f"pcl{iter:05d}.pcd"), pcl, compressed=True)
	outPath = outDir / f"pcl{iter:05d}.npz"
	np.savez_compressed(str(outPath), np.array([pcl.points, pcl.colors], dtype=np.float32))

	return outPath


# ==================================================================================================
def filter_events(events, ts):
	"""Removes all events with timestamp lower than the specified one

	Args:
		events (list): the list of events in form of (x, y, t, p)
		ts (float): the timestamp to split events

	Return:
		(list): a list of events with timestamp above the threshold
	"""
	tss = np.array([e[2] for e in events])
	idxArray = np.argsort(tss) # I hope it"s not needed
	i = np.searchsorted(tss[idxArray], ts)
	return [events[k] for k in idxArray[i:]]


# ==================================================================================================
def generate_danger(depth, prevDepth, iter: int, outDir, flow, prevFlow) -> Path:
	if prevDepth is None and prevFlow is None:
		warning("prevDepth and prevFlow are None")
		return None

	# Flow is pixels/second floating point
	depth[np.isinf(depth)] = -1
	prevDepth[np.isinf(prevDepth)] = -1

	# cv2.remap LOOKS UP every pixel with a coordinate, we can't 'apply' the optical flow as a transform directly.
	# So the optical flow needs to be registered with the target depth for that frame, otherwise we try to look up pixels for which have no optical flow!
	# So... we need the future depth frame and its optical flow to tell us how that depth maps to this frame
	# Or more accurately, we need the current depth frame and its optical flow to tell us how that depth frame maps the the PREVIOUS depth frame
	coords = np.indices(prevDepth.shape).transpose(1, 2, 0).astype(np.float32)
	coords = coords[:, :, ::-1]
	coords -= (flow / framerate).transpose(1, 2, 0)
	prevDepthWarped = cv2.remap(prevDepth, coords, None, interpolation=cv2.INTER_NEAREST, borderMode=cv2.BORDER_CONSTANT, borderValue=-1)
	# Although, the optical flow VALUES I think are not perfectly aligned with the time frame (maybe they tell us about the instantaneous velocity, not between the last two frames)
	# The issue with this is that a few discontinuities are propogated and duplicated.
	# First we can definitely re-exclude where we don't currently have depth
	prevDepthWarped[depth <= 0] = 0
	danger = prevDepthWarped - depth # Moving closer at m/frame
	danger = danger * framerate # Moving closer at m/s
	danger[danger < 0] = 0 # We only care about positive values

	# We don't know these values, allow downstream tasks to ignore these
	# To filter the most severe remaining positive discontinuity problems, things getting closer very quickly is unrealistic
	danger[danger > maxVelFilter] = -1 # m/s
	danger = danger / (depth + eps) # Danger is now 1/s
	danger[depth <= 0] = -1
	danger[prevDepthWarped <= 0] = -1

	outPath = outDir / f"danger{iter:05d}.npz"
	np.savez_compressed(str(outPath), danger.astype(np.float32))

	# DEBUG stuff
	# cv2.imwrite(str(outPath), danger)
	# danger = danger * 255
	# danger[danger > 255] = 255
	# outPath0 = outDir / f"danger{iter:05d}_.png"
	# cv2.imwrite(str(outPath0), danger)
	# depth = depth * 255
	# depth[depth > 255] = 255
	# outPath1 = outDir / f"depth{iter:05d}.png"
	# cv2.imwrite(str(outPath1), depth)
	# outPath2 = outDir / f"prevDepthWarped{iter:05d}.png"
	# prevDepthWarped = prevDepthWarped * 255
	# prevDepthWarped[prevDepthWarped > 255] = 255
	# cv2.imwrite(str(outPath2), prevDepthWarped)

	return outPath


# ==================================================================================================
# def save_to_lmdb(env, path: Path, iter: int, bagPath: Path, keys: set, keep: bool = False):
# def save_to_lmdb(env, path: Path, iter: int, keys: set, keep: bool = False):
def save_to_lmdb(env, path: Path, keys: set, keep: bool = False, offset: int = 0):
	if path is None:
		warning("Cannot write empty path")
		return
	# key = f"{bagPath.stem}/{iter}"
	key = str(len(keys) + offset)
	keys.add(key)
	with env.begin(write=True) as txn:
		with open(path, mode="rb") as file:
			txn.put(
				key=pickle.dumps(key),
				value=file.read(),
				dupdata=False,
			)
	if not keep:
		path.unlink()


# ==================================================================================================
def parse_args():
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("input_path", type=Path,
						help="Directory to search for ROS bags, or path to a ROS bag")
	parser.add_argument("--max_aug",
						help="Maximum number of images to combine for augmentation.",
						type=int,
						default=6)
	parser.add_argument("--n_skip",
						help="Maximum number of images to combine for augmentation.",
						type=int,
						default=1)
	parser.add_argument("-f", "--force", action="store_true", help="Overwrite output LMDB if any part of it exists")
	parser.add_argument("-d", "--debug", action="store_true", help="Only do 0.5 seconds as a test")
	parser.add_argument("-k", "--keep", action="store_true", help="Don't remove intermediate files")

	return parser.parse_args()


# ==================================================================================================
if __name__ == "__main__":
	main(parse_args())
