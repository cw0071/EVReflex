#!/usr/bin/env python
import argparse
from pathlib import Path
import lmdb
import pickle
from tqdm import tqdm
from kellog import info, warning, error, debug
from natsort import natsorted

dataTypes = ["danger", "event", "flow", "img", "pcl", "seg"]
skip = [
	"scene0000_00_vh_clean_2",
	"scene0080_00_vh_clean_2",
	"scene0116_00_vh_clean_2",
	"scene0135_00_vh_clean_2",
	"scene0150_00_vh_clean_2",
	"scene0153_00_vh_clean_2",
	"scene0182_00_vh_clean_2",
	"scene0195_00_vh_clean_2",
	"scene0237_00_vh_clean_2",
	"scene0250_00_vh_clean_2",
	"scene0415_00_vh_clean_2",
	"scene0444_00_vh_clean_2",
	"scene0448_00_vh_clean_2",
	"scene0457_00_vh_clean_2",
	"scene0485_00_vh_clean_2",
	"scene0570_00_vh_clean_2",
	"scene0583_00_vh_clean_2",
	"scene0676_00_vh_clean_2",
	"scene0684_00_vh_clean_2",
]

# ==================================================================================================
def main(args):
	global dataTypes # Not fully sure why I need this here
	args.output_path.mkdir(parents=True, exist_ok=True)

	scenes = natsorted(list(args.input_path.glob("scene*_00_vh_clean_2")))
	# scenes = scenes[:5] # DEBUG
	# scenes = scenes[:2] # DEBUG

	if args.data_type is not None:
		dataTypes = [args.data_type]

	if not args.force and any([(args.output_path / dataType).exists() for dataType in dataTypes]):
		warning(f"Output lmdb(s) exists in '{args.output_path}'")
		ans = input("Overwrite? [Y/n] ").lower()
		if ans not in ["", "y"]:
			warning("Not overwriting, aborting")
			quit(0)
	info(f"Saving output lmdbs in '{args.output_path}'")

	bar = tqdm(dataTypes, position=0, leave=True)
	for dataType in bar:
		outputKeys = set()
		outputDB = args.output_path / dataType
		bar1 = tqdm(scenes, position=1, leave=True)
		with lmdb.open(path=str(outputDB), map_size=2 ** 40) as outputEnv:
			for scene in bar1:
				if scene.name in skip:
					warning(f"Skipping '{scene.name}'")
					continue
				bar.set_description(dataType) # Update screen at the same time as the lower bar
				bar1.set_description(scene.name)
				inputDB = scene / dataType
				with lmdb.open(str(inputDB), readonly=True) as inputEnv:
					with inputEnv.begin() as inputTxn:
						inKeys = pickle.loads(inputTxn.get(key=pickle.dumps("keys")))
						for i, key in enumerate(tqdm(inKeys, position=2, leave=True)):
							if key == "1":
								warning("Skipping first key because it's not present in danger lmdb...")
								continue
							if i == len(inKeys) - 1 and dataType == "danger":
								warning("Skipping last key in danger lmdb because it's extra...")
								continue
							inputKey = pickle.dumps(key)
							outputKey = str(len(outputKeys))
							outputKeys.add(outputKey)
							outputKey = pickle.dumps(outputKey)
							with outputEnv.begin(write=True) as outputTxn:
								outputTxn.put(
									key=outputKey,
									value=inputTxn.get(inputKey),
									dupdata=False,
								)
			with outputEnv.begin(write=True) as outputTxn:
				outputTxn.put(
					key=pickle.dumps("keys"),
					value=pickle.dumps(outputKeys),
					dupdata=False,
				)


# ==================================================================================================
def parse_args():
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("input_path", type=Path, help="Input LMDB(s) directory")
	parser.add_argument("output_path", type=Path, help="Output LMDB directory")
	parser.add_argument("-d", "--data_type", choices=dataTypes, type=str, help="If specified, limit to a particular data type", default=None)
	parser.add_argument("-f", "--force", action="store_true", help="Overwrite output LMDB if any part of it exists")

	return parser.parse_args()


# ==================================================================================================
if __name__ == "__main__":
	main(parse_args())
