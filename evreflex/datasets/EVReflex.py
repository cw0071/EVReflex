#!/usr/bin/env python
import torch
from torch.utils.data import Dataset
import torchvision.transforms.functional as F
from PIL import Image
import numpy as np
from pathlib import Path
from natsort import natsorted
import torchvision.transforms as tfs

from evreflex.datasets.database import ImageDatabase, ArrayDatabase#, PointcloudDatabase

from typing import Optional

TRAIN = 0.7
VAL = 0.15
TEST = 0.15 # Not actually used, it's the remainer of the other two

# ==================================================================================================
class EVReflex(Dataset):
	# ----------------------------------------------------------------------------------------------
	def __init__(
		self,
		data_path: Path,
		split: Optional[str] = None,
		count_only: bool = False,
		time_only: bool = False,
		skip_frames: bool = False,
		transforms=[[]] * 4,
		vis: bool = True,
		subset: Optional[bool] = None,
		include_img: bool = False,
		include_ev: bool = False,
		dynamic: bool = False,
	):
		self.data_path = data_path
		self.flow_path = data_path / "flow"
		self.pcl_path = data_path / "pcl"
		self.danger_path = data_path / "danger"
		self.img_path = data_path / "img"
		self.ev_path = data_path / "event"
		self.split = split
		self.count_only = count_only
		self.time_only = time_only
		self.skip_frames = skip_frames
		self.transforms = transforms
		self.vis = vis
		self.subset = subset
		self.dynamic = dynamic

		self.db_flow = ArrayDatabase(self.flow_path)
		self.db_pcl = ArrayDatabase(self.pcl_path)
		self.db_danger = ArrayDatabase(self.danger_path)
		self.db_img = ImageDatabase(self.img_path, mode="L") if include_img else None
		self.db_ev = ArrayDatabase(self.ev_path) if include_ev else None

		if self.count_only or self.time_only:
			raise NotImplementedError("See `_read_evs()`")

	# ----------------------------------------------------------------------------------------------
	def __len__(self):
		if self.db_img:
			self.length = min(len(self.db_flow), len(self.db_pcl), len(self.db_danger), len(self.db_img))
		else:
			self.length = min(len(self.db_flow), len(self.db_pcl), len(self.db_danger))

		if self.split is None:
			length = self.length
		elif self.split == "train":
			length = int(self.length * TRAIN)
		elif self.split == "val":
			length = int(self.length * VAL)
		elif self.split == "test":
			length = self.length - int(self.length * TRAIN) - int(self.length * VAL) - 1

		if self.subset is not None:
			assert self.subset <= 1.0
			return int(length * self.subset)

		# FIX check if this is still needed. May have been fixed in merged LMDBs
		# Accounts for danger keys starting at "2", or to provide sequential depth images with each index?
		length -= 1

		return length

	# ----------------------------------------------------------------------------------------------
	def __getitem__(self, index):
		if self.split == "val":
			index = index + int(self.length * TRAIN)
		elif self.split == "test":
			index = index + int(self.length * TRAIN) + int(self.length * VAL)
		# EVDodge:
		# It's the stacking of {E+, E-, E_t}:
		# 1. Per-pixel average number of _positive_ event triggers in the spatio-temporal window spanned between t_0 and t_0 + d_t
		# 2. Per-pixel average number of _negative_ event triggers in the spatio-temporal window spanned between t_0 and t_0 + d_t
		# 3. Average trigger time per pixel

		# FIX check if this is still needed. May have been fixed in merged LMDBs
		# Accounts for danger keys starting at "2", or to provide sequential depth images with each index?
		# index += 1
		index += 2
		flow = self.db_flow[index]
		points = self.db_pcl[index]
		if self.dynamic:
			points2 = self.db_pcl[index + 1]
			points = np.vstack([points, points2])
		danger = self.db_danger[index]
		if self.db_img:
			img = self.db_img[index]
		else:
			img = torch.empty(0)
		if self.db_ev:
			# NOTE this follows EVDodge's format
			events = self.db_ev[index]
			event_count_images = torch.from_numpy(events[0].astype(np.int16))
			event_time_images = torch.from_numpy(events[1].astype(np.float32))
			image_times = torch.from_numpy(events[2].astype(np.float64))
			event_count_image1, event_time_image1 = self.__read_evs(event_count_images, event_time_images, 0)
			events1 = torch.stack([event_count_image1[0], event_count_image1[1], event_time_image1[0]])
			event_count_image2, event_time_image2 = self.__read_evs(event_count_images, event_time_images, 1)
			events2 = torch.stack([event_count_image2[0], event_count_image2[1], event_time_image2[0]])
			events = torch.vstack([events1, events2])
		else:
			events = torch.empty(0)

		for transform in self.transforms[0]:
			flow = transform(flow)
		for transform in self.transforms[1]:
			points = transform(points)
		for transform in self.transforms[2]:
			danger = transform(danger)
		# FIX this is badly written code
		if isinstance(img, Image.Image):
			for transform in self.transforms[3]:
				img = transform(img)
		if isinstance(events, np.ndarray):
			for transform in self.transforms[4]:
				events = transform(events)

		return flow, points, danger, img, events

	# ----------------------------------------------------------------------------------------------
	def _read_evs(self, ev_count_imgs, ev_time_imgs, n_frames):
		# ev_count_imgs = ev_count_imgs.reshape(shape).type(torch.float32)
		ev_count_img = ev_count_imgs[:n_frames, :, :, :]
		ev_count_img = torch.sum(ev_count_img, dim=0).type(torch.float32)
		ev_count_img = ev_count_img.permute(2, 0, 1)

		# ev_time_imgs = ev_time_imgs.reshape(shape).type(torch.float32)
		ev_time_img = ev_time_imgs[:n_frames, :, :, :]
		ev_time_img = torch.max(ev_time_img, dim=0)[0]

		ev_time_img /= torch.max(ev_time_img)
		ev_time_img = ev_time_img.permute(2, 0, 1)

		"""
		if self.count_only:
			ev_img = ev_count_img
		elif self.time_only:
			ev_img = ev_time_img
		else:
			ev_img = torch.cat([ev_count_img, ev_time_img], dim=2)

		ev_img = ev_img.permute(2,0,1).type(torch.float32)
		"""

		return ev_count_img, ev_time_img

	# ----------------------------------------------------------------------------------------------
	def __read_evs(self, ev_count_imgs, ev_time_imgs, frame):
		ev_count_img = ev_count_imgs[frame:frame + 1, :, :, :]
		ev_count_img = torch.sum(ev_count_img, dim=0).type(torch.float32)
		ev_count_img = ev_count_img.permute(2, 0, 1)

		ev_time_img = ev_time_imgs[frame:frame + 1, :, :, :]
		ev_time_img = torch.max(ev_time_img, dim=-1, keepdim=True)[0].squeeze(0)

		ev_time_img /= torch.max(ev_time_img)
		ev_time_img = ev_time_img.permute(2, 0, 1)

		"""
		if self.count_only:
			ev_img = ev_count_img
		elif self.time_only:
			ev_img = ev_time_img
		else:
			ev_img = torch.cat([ev_count_img, ev_time_img], dim=2)

		ev_img = ev_img.permute(2,0,1).type(torch.float32)
		"""

		return ev_count_img, ev_time_img


	# ----------------------------------------------------------------------------------------------
	# Argument `points` has 3 channels (dimension 1)
	# Position (X, Y, Z) (dimension 3 == 0)
	# colour (C, _, _) (dimension 3 == 1)
	# If sequential, dimension 3 == 2, 3 is the same, for next frame t+1
	@staticmethod
	def points_2_depth(points, sequential: bool):
		if sequential:
			depth1 = points[:, 2:3, :, 0]
			depth2 = points[:, 2:3, :, 2]
			depth1 = depth1.reshape(*depth1.shape[:2], 260, 346)
			depth2 = depth2.reshape(*depth2.shape[:2], 260, 346)
			depth = torch.hstack([depth1, depth2]) # [B, 2, H, W]
		else:
			depth = points[:, 2:3, :, 0]
			depth = depth.reshape(*depth.shape[:2], 260, 346)

		return depth


# ==================================================================================================
if __name__ == "__main__":
	import colored_traceback.auto
	import argparse
	from torch.utils.data import DataLoader
	from kellog import info, warning, error, debug
	import cv2
	import open3d as o3d

	parser = argparse.ArgumentParser()
	parser.add_argument("data_path", type=Path, help="Path to EVReflex dataset, either LMDB directory or `.txt` list of relative paths")
	args = parser.parse_args()

	if args.data_path.is_file():
		with open(args.data_path, "r") as txt:
			dirs = txt.read().splitlines()
		dataset = torch.utils.data.ConcatDataset(
			[EVReflex(
				data_path=args.data_path.parent / d,
				split="train",
				include_img=True,
				include_ev=True,
				transforms=[
					[],
					[],
					[tfs.ToTensor()],
					[tfs.ToTensor()],
					[],
				]
			) for d in dirs]
		)
	elif args.data_path.is_dir():
		dataset = EVReflex(
			data_path=args.data_path,
			split="train",
			include_img=True,
			include_ev=True,
			transforms=[
				[],
				[],
				[tfs.ToTensor()],
				[tfs.ToTensor()],
				[],
			]
		)
	else:
		raise ValueError(f"Expected data_path '{args.data_path}' to be a file or directory")

	dataloader = DataLoader(dataset=dataset, batch_size=1, shuffle=False)
	info(f"Length: {len(dataloader)}")
	info("Press 'q' or escape to quit, any other key to advance")
	for i, (flow, pcl, danger, img) in enumerate(dataloader):
		print(f"Showing {i + 1}/{len(dataloader)}")
		flow = flow[0].numpy()
		danger = danger[0].numpy()
		pcl = pcl[0]
		flow = flow[0].squeeze() # x
		danger = danger.squeeze()

		# Output is white but reports no error if I don't do this
		pcl[torch.isinf(pcl)] = 0
		pcl[torch.isnan(pcl)] = 0

		pcd = o3d.geometry.PointCloud()
		pcd.points = o3d.utility.Vector3dVector(pcl[0])
		pcd.colors = o3d.utility.Vector3dVector(pcl[1])
		o3d.visualization.draw_geometries([pcd])
		cv2.imshow("flow", flow)
		cv2.imshow("danger", danger)
		key = cv2.waitKey(0)
		if key == ord("q") or key == 27: # 27 = escape
			break
