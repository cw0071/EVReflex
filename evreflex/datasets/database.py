#!/usr/bin/env python3
from pathlib import Path
import io
import lmdb
import pickle
from PIL import Image
from PIL import ImageFile
import numpy as np
import open3d as o3d
import tempfile
from kellog import info, warning, error, debug

ImageFile.LOAD_TRUNCATED_IMAGES = True

# ==================================================================================================
class Database(object):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, path: Path):
		self.path = path
		if not self.path.exists():
			raise FileNotFoundError(self.path)
		self.db = lmdb.open(
			path=str(self.path),
			readonly=True,
			readahead=False,
			max_spare_txns=128,
			lock=False,
		)
		with self.db.begin() as txn:
			keys = pickle.loads(txn.get(key=pickle.dumps("keys")))
		self.keys = set(keys)

	# ----------------------------------------------------------------------------------------------
	def __iter__(self):
		return iter(self.keys)

	# ----------------------------------------------------------------------------------------------
	def __len__(self):
		return len(self.keys)

	# ----------------------------------------------------------------------------------------------
	def __getitem__(self, item):
		key = pickle.dumps(item)
		with self.db.begin() as txn:
			value = txn.get(key)

		return value

	# ----------------------------------------------------------------------------------------------
	def __del__(self):
		self.db.close()


# ==================================================================================================
class ImageDatabase(Database):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, path: Path, mode: str="RGB"):
		super().__init__(path)
		self.mode = mode

	# ----------------------------------------------------------------------------------------------
	def __getitem__(self, item):
		try:
			key = pickle.dumps(str(item))
			with self.db.begin() as txn:
				image = Image.open(io.BytesIO(txn.get(key))).convert(mode=self.mode)
		except OSError:
			error(f"Failed to read '{self.path.stem}' database at index {item}")
			raise

		return image


# ==================================================================================================
class ArrayDatabase(Database):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, path: Path):
		super().__init__(path)

	# ----------------------------------------------------------------------------------------------
	def __getitem__(self, item):
		try:
			key = pickle.dumps(str(item))
			with self.db.begin() as txn:
				with io.BytesIO(txn.get(key)) as f:
					array = np.load(f, allow_pickle=True, encoding="bytes")
					if isinstance(array, np.ndarray):
						pass
					elif isinstance(array, np.lib.npyio.NpzFile):
						array = array.f.arr_0
					else:
						raise RuntimeError(f"Unexpected type array type '{type(array)}'")
		except OSError:
			error(f"Failed to read '{self.path.stem}' database at index {item}")
			raise

		return array


# ==================================================================================================
class PointcloudDatabase(Database):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, path: Path):
		raise NotImplementedError("Use ArrayDatabase and numpy instead...")
		super().__init__(path)

	# ----------------------------------------------------------------------------------------------
	def __getitem__(self, item):
		key = pickle.dumps(str(item))
		with self.db.begin() as txn:
			# o3d.io can't read from open files, grr.
			with tempfile.NamedTemporaryFile(suffix=".pcd") as f:
			# with tempfile.SpooledTemporaryFile() as f:
				f.write(txn.get(key))
				f.seek(0)
				pcl = o3d.io.read_point_cloud(f.name)

		return np.asarray(pcl.points)


# ==================================================================================================
class LabelDatabase(Database):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, path: Path):
		super().__init__(path)
		raise NotImplementedError

	# ----------------------------------------------------------------------------------------------
	def __getitem__(self, item):
		# key = pickle.dumps(str(item))
		key = pickle.dumps(item)
		with self.db.begin() as txn:
			label = pickle.loads(txn.get(key))

		return label
