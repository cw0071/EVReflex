#!/usr/bin/env python
import pytorch_lightning as pl
import torch
from torch import nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, ConcatDataset
import torchvision.transforms as tfs
from pathlib import Path
import time
from kellog import info, warning, error, debug
import evreflex.utils
from PIL import Image

from typing import Sequence, Optional

from evreflex.datasets import EVReflex
from evreflex.models.layers import ConvolutionBlock, ResidualConvolutionBlock, ResidualTransposedConvolutionBlock, TransposedConvolutionBlock
from evreflex import utils

eps = 1e-12

# ==================================================================================================
class EVReflexNet(pl.LightningModule):
	# ----------------------------------------------------------------------------------------------
	def __init__(
		self,
		blocks: Sequence[int],
		operation: str,
		base_channels: int = 8,
		train_path: Optional[Path] = None,
		val_path: Optional[Path] = None,
		test_path: Optional[Path] = None,
		batch_size: int = 1,
		workers: int = 0,
		lr: float = 0.01,
		subset: Optional[bool] = None
	):
		super().__init__()
		self.operation = operation
		self.train_path = train_path
		self.val_path = val_path
		self.test_path = test_path
		self.batch_size = batch_size
		self.workers = workers
		self.lr = lr
		self.subset = subset

		# example_input_array is required to log the graph to tensorboard
		if self.operation == "depth":
			num_classes = 1
			self.example_input_array = (torch.zeros(1, 1, 260, 344), torch.empty(0))
		elif self.operation == "flow":
			num_classes = 2
			self.example_input_array = (torch.empty(0), torch.zeros(1, 2, 260, 344))
		elif self.operation == "both":
			num_classes = 3
			self.example_input_array = (torch.zeros(1, 1, 260, 344), torch.zeros(1, 2, 260, 344))
		elif self.operation == "dynamic_depth":
			num_classes = 2
			self.example_input_array = (torch.zeros(1, 2, 260, 344), torch.empty(0))
		elif self.operation == "dynamic_both":
			num_classes = 4
			self.example_input_array = (torch.zeros(1, 2, 260, 344), torch.zeros(1, 2, 260, 344))
		else:
			raise NotImplementedError(f"{self.operation} is not a known operation")

		# Point cloud
		layers = []
		layers += [ConvolutionBlock(
			num_layers=blocks[0],
			in_channels=num_classes,
			out_channels=base_channels,
		)]
		layers += [ResidualConvolutionBlock(
			num_layers=blocks[1],
			in_channels=base_channels,
			mid_channels=base_channels,
			out_channels=base_channels * 2 ** 2,
		)]
		layers += [ResidualTransposedConvolutionBlock(
			num_layers=blocks[1],
			in_channels=base_channels * 2 ** 2,
			mid_channels=base_channels,
			out_channels=base_channels,
		)]
		layers += [TransposedConvolutionBlock(
			num_layers=blocks[0],
			in_channels=base_channels,
			out_channels=1,
		)]

		# self.layers = nn.Sequential(*layers)
		self.layers = layers
		self.l1 = layers[0]
		self.l2 = layers[1]
		self.l3 = layers[2]
		self.l4 = layers[3]
		self.num_classes = num_classes
		self.out_channels = layers[-2].out_channels

		self.criterion = nn.MSELoss(reduction="none")

		self.elapsed = 0 # Seconds
		self.steps = 0 # Train steps
		self.elapsedInterval = 10 # Seconds
		self.elapsedSteps = 0 # Counter
		self.start = None

	# ----------------------------------------------------------------------------------------------
	def configure_optimizers(self):
		warning("Actually choose a sensible optimiser") # TODO
		optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
		lr_scheduler = {
			"scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer),
			"monitor": "train_loss",
		}

		return [optimizer], [lr_scheduler]

	# ----------------------------------------------------------------------------------------------
	# Inference
	def forward(self, depth, flow):
		if self.operation in ["depth", "dynamic_depth"]:
			x = self.l1(depth)
		elif self.operation == "flow":
			x = self.l1(flow)
		elif self.operation in ["both", "dynamic_both"]:
			x = self.l1(torch.hstack([depth, flow]))
		x = self.l2(x)
		x = self.l3(x)
		x = self.l4(x)

		return x

	# ----------------------------------------------------------------------------------------------
	def on_train_start(self):
		if self.logger:
			tensorboard = self.logger.experiment
			tensorboard.add_text("Git revision", evreflex.utils.get_git_rev())

	# ----------------------------------------------------------------------------------------------
	def step(self, batch, batch_idx, dataset):
		flow, points, danger, img, events = batch # FIX img is just visualised, but it's using GPU memory here

		points = points.permute(0, 3, 2, 1)
		depth = dataset.points_2_depth(points, self.operation in ["dynamic_depth", "dynamic_both"])

		# Janky bit to make resolution divisible by 4
		depth = depth[:, :, :, 1:-1]
		danger = danger[:, :, :, 1:-1]
		flow = flow[:, :, :, 1:-1]
		if isinstance(img, Image.Image):
			img = img[:, :, :, 1:-1]
		if events.numel():
			events = events[:, :3, :, 1:-1]

		depth[torch.isnan(depth)] = 0 # There may be NaNs! So we need to set them to something else

		output = self(depth, flow)

		return output, flow, depth, danger, img, events

	# ----------------------------------------------------------------------------------------------
	def training_step(self, batch, batch_idx):
		if self.start is not None:
			self.elapsed += time.time() - self.start
			self.steps += 1
			if self.elapsed >= self.elapsedInterval:
				# self.log("iteration_speed", self.steps / self.elapsed, on_epoch=False, on_step=True)
				if self.logger:
					tensorboard = self.logger.experiment
					tensorboard.add_scalar("sample_speed", self.steps / self.elapsed * self.batch_size, global_step=self.elapsedSteps)
				self.elapsed = 0
				self.steps = 0
				self.elapsedSteps += 1
		self.start = time.time()

		output, flow, depth, danger, img, events = self.step(batch, batch_idx, self.train_dataloader().dataset)
		loss = self.criterion(output, danger)

		if batch_idx == 0 and self.logger:
		# if batch_idx == 0 and self.logger and self.current_epoch % 10 == 0:
			flow = flow.detach().cpu()
			output = output.detach().cpu()
			tensorboard = self.logger.experiment
			tensorboard.add_images("OP", output.clamp(0, 1), global_step=self.current_epoch)
			depth[depth == 0] = float("Inf") # So when we invert the visualisation the background is black
			tensorboard.add_images("Inverse depth", ((1 / depth) / 5).clamp(0, 1), global_step=self.current_epoch) # Black/white polarity
			tensorboard.add_images("Optical flow", utils.flow_viz(flow), global_step=self.current_epoch)
			tensorboard.add_images("Ground truth TTI", danger.clamp(0, 1), global_step=self.current_epoch)
			if isinstance(img, Image.Image):
				tensorboard.add_images("img", img, global_step=self.current_epoch)
			tensorboard.add_images("events", events, global_step=self.current_epoch)

		loss = loss.mean() # Do this if reduce in criterion is "none"
		self.log("train_loss", loss, on_epoch=True, on_step=False)
		if torch.isnan(loss).any() or torch.isinf(loss).any():
			error("NaNs in train loss!")

		return loss

	# ----------------------------------------------------------------------------------------------
	def validation_step(self, batch, batch_idx):
		output, flow, depth, danger, img, events = self.step(batch, batch_idx, self.val_dataloader().dataset)
		loss = self.criterion(output, danger)
		loss = loss.mean() # Do this if reduce in criterion is "none"
		self.log(f"val_loss", loss, on_epoch=True, on_step=False)
		if torch.isnan(loss).any() or torch.isinf(loss).any():
			error("NaNs in val loss!")

		return loss

	# ----------------------------------------------------------------------------------------------
	def test_step(self, batch, batch_idx):
		output, flow, depth, danger, img, events = self.step(batch, batch_idx, self.test_dataloader().dataset)

		ffv = 100 # Fudge factor for visualisation
		thresh = 0.001

		if self.logger:
			tensorboard = self.logger.experiment
			output_ = output * ffv
			output_[output_ < 0] = 0
			output_[output_ > 1] = 1
			danger_ = danger * ffv
			danger_[danger_ < 0] = 0
			danger_[danger_ > 1] = 1
			for i, sample in enumerate(output_):
				tensorboard.add_image("OP", sample, global_step=(batch_idx * events.shape[0]) + i)
			for i, sample in enumerate(danger_):
				tensorboard.add_image("GT", sample, global_step=(batch_idx * events.shape[0]) + i)
			for i, sample in enumerate(events):
				tensorboard.add_image("events", sample, global_step=(batch_idx * events.shape[0]) + i)

		outputBinary = output.clone()
		outputBinary[outputBinary < 0] = 0
		# tp = tfs.ToPILImage()
		threshDanger = torch.where(danger > thresh, 1, 0).byte()
		threshOutput = torch.where(outputBinary > thresh, 1, 0).byte()

		if self.logger:
			tensorboard = self.logger.experiment
			tensorboard.add_images("OP thresh", threshOutput * 255, global_step=batch_idx)
			tensorboard.add_images("GT thresh", threshDanger * 255, global_step=batch_idx)

		iou = utils.calc_iou(threshOutput, threshDanger)

		loss = self.criterion(output, danger)
		lossDepth = self.criterion(output, 1 / (depth[:, :1] + eps))
		self.log(f"test_loss", loss.mean(), on_epoch=True, on_step=False) # Do this if reduce in criterion is "none"
		self.log(f"test_loss_depth", lossDepth.mean(), on_epoch=True, on_step=False) # Do this if reduce in criterion is "none"
		self.log(f"test_thresh_iou", iou.mean(), on_epoch=True, on_step=False)
		# TODO grid search best depth threshold to use as IoU baseline?
		# self.log(f"test_thresh_iou_depth", iou.mean(), on_epoch=True, on_step=False)

		# return iou.mean() # Does nothing?

	# ----------------------------------------------------------------------------------------------
	def get_dataset(self, dataPath: Path, split: str) -> EVReflex:
		return EVReflex(
			data_path=dataPath,
			split=split,
			transforms=[
				[],
				[],
				[tfs.ToTensor()],
				[tfs.ToTensor()],
				[],
			],
			subset=self.subset,
			include_ev=True,
			dynamic=self.operation in ["depth", "dynamic_depth"],
		)

	# ----------------------------------------------------------------------------------------------
	def train_dataloader(self) -> DataLoader:
		if self.train_path.is_file():
			with open(self.train_path, "r") as txt:
				dirs = txt.read().splitlines()
			dataset = ConcatDataset([self.get_dataset(self.train_path.parent / d) for d in dirs])
		else:
			dataset = self.get_dataset(self.train_path, "train")

		return DataLoader(
			dataset,
			batch_size=self.batch_size,
			shuffle=True,
			num_workers=self.workers,
			pin_memory=self.device != torch.device("cpu")
		)

	# ----------------------------------------------------------------------------------------------
	def val_dataloader(self) -> DataLoader:
		if self.val_path.is_file():
			with open(self.val_path, "r") as txt:
				dirs = txt.read().splitlines()
			dataset = ConcatDataset([self.get_dataset(self.val_path.parent / d) for d in dirs])
		else:
			dataset = self.get_dataset(self.val_path, "val")

		return DataLoader(
			dataset,
			batch_size=self.batch_size,
			shuffle=False,
			num_workers=self.workers,
			pin_memory=self.device != torch.device("cpu")
		)

	# ----------------------------------------------------------------------------------------------
	def test_dataloader(self) -> DataLoader:
		if self.test_path.is_file():
			with open(self.test_path, "r") as txt:
				dirs = txt.read().splitlines()
			dataset = ConcatDataset([self.get_dataset(self.test_path.parent / d) for d in dirs])
		else:
			dataset = self.get_dataset(self.test_path, "test")

		return DataLoader(
			dataset,
			batch_size=self.batch_size,
			shuffle=False,
			num_workers=self.workers,
			pin_memory=self.device != torch.device("cpu")
		)


# ==================================================================================================
if __name__ == "__main__":
	import colored_traceback.auto
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("--data_dir", type=Path, help="Path to EVReflex dataset")
	args = parser.parse_args()

	# Blocks:
	# - Number of layers in the first ConvolutionBlock
	# - Number of layers in the subsequent ResidualConvolutionBlocks
	model = EVReflexNet(
		blocks=[3, 2],
		# num_classes=40,
		num_classes=4, # DEBUG
		base_channels=8,
	)

	if not args.data_dir:
		warning("Using random input, specify `--data_dir` to load from a dataset")
		# points = torch.rand(1, 40, 89960)
		points = torch.rand(1, 4, 89960)
		flow = torch.rand(1, 2, 260, 346)
	else:
		dataset = EVReflex(
			data_path=args.data_dir,
			transforms=[
				[],
				[],
				[tfs.ToTensor()],
				[tfs.ToTensor()],
				[],
			]
		)
		dataloader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=True)
		for sample in dataloader:
			flow, points, danger, img, events = sample
			# debug(list(flow.shape), list(points.shape), list(danger.shape))
			break
	debug(f"Input shape: {points.shape}")

	points = points.reshape([*points.shape[:2], 260, 346])
	points = points[:, :, :, 1:-1] # Crop to make divisible by 4
	output = model(points, flow)
	debug(f"Output shape: {output.shape}")
