#!/usr/bin/env python3
from torch import nn, Tensor

# ==================================================================================================
class Layer(nn.Module):
	# ----------------------------------------------------------------------------------------------
	def __init__(self):
		super().__init__()

	# ----------------------------------------------------------------------------------------------
	def forward(self, x: Tensor) -> Tensor:
		return self.layers(x)


# ==================================================================================================
class ConvolutionLayer(Layer):
	def __init__(
		self,
		in_channels: int,
		out_channels: int,
		kernel_size: int,
		stride: int = 1,
		padding: int = 0,
		norm: bool = True,
		activation: bool = True,
	):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size
		self.stride = stride
		self.padding = padding
		self.norm = norm
		self.activation = activation

		layers = []
		layer = nn.Conv2d(
			in_channels=in_channels,
			out_channels=out_channels,
			kernel_size=kernel_size,
			stride=stride,
			padding=padding,
			bias=not norm,
		)
		layers += [layer]
		if norm:
			layer = nn.BatchNorm2d(num_features=out_channels)
			layers += [layer]
		if activation:
			layer = nn.CELU(inplace=True)
			layers += [layer]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class TransposedConvolutionLayer(Layer):
	def __init__(
		self,
		in_channels: int,
		out_channels: int,
		kernel_size: int,
		stride: int = 1,
		padding: int = 0,
		output_padding: int = 0,
		norm: bool = True,
		activation: bool = True,
	):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.kernel_size = kernel_size
		self.stride = stride
		self.padding = padding
		self.output_padding = output_padding
		self.norm = norm
		self.activation = activation

		layers = []
		layer = nn.ConvTranspose2d(
			in_channels=in_channels,
			out_channels=out_channels,
			kernel_size=kernel_size,
			stride=stride,
			padding=padding,
			output_padding=output_padding,
			bias=not norm,
		)
		layers += [layer]
		if norm:
			layer = nn.BatchNorm2d(num_features=out_channels)
			layers += [layer]
		if activation:
			layer = nn.CELU(inplace=True)
			layers += [layer]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class Identity(nn.Module):
	# ----------------------------------------------------------------------------------------------
	def __init__(self):
		super().__init__()

	# ----------------------------------------------------------------------------------------------
	def forward(self, x: Tensor) -> Tensor:
		return x


# ==================================================================================================
class ConvolutionBlock(Layer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, num_layers: int, in_channels: int, out_channels: int):
		super(Layer, self).__init__()

		self.num_layers = num_layers
		self.in_channels = in_channels
		self.out_channels = out_channels

		layers = [
			ConvolutionLayer(
				in_channels=in_channels,
				out_channels=out_channels,
				kernel_size=3,
				stride=2,
				padding=1,
			)
		] + [
			ConvolutionLayer(
				in_channels=out_channels,
				out_channels=out_channels,
				kernel_size=3,
				padding=1,
			)
			for _ in range(1, num_layers)
		]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class TransposedConvolutionBlock(Layer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, num_layers: int, in_channels: int, out_channels: int):
		super(Layer, self).__init__()

		self.num_layers = num_layers
		self.in_channels = in_channels
		self.out_channels = out_channels

		layers = [
			TransposedConvolutionLayer(
				in_channels=in_channels,
				out_channels=out_channels,
				kernel_size=3,
				stride=2,
				padding=1,
				output_padding=1,
			)
		] + [
			TransposedConvolutionLayer(
				in_channels=out_channels,
				out_channels=out_channels,
				kernel_size=3,
				padding=1,
			)
			for _ in range(1, num_layers)
		]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class ConvolutionBranch(Layer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, mid_channels: int, out_channels: int, stride: int):
		super().__init__()

		self.in_channels = in_channels
		self.mid_channels = mid_channels
		self.out_channels = out_channels
		self.stride = stride

		layers = [
			ConvolutionLayer(
				in_channels=in_channels, out_channels=mid_channels, kernel_size=1
			),
			ConvolutionLayer(
				in_channels=mid_channels,
				out_channels=mid_channels,
				kernel_size=3,
				stride=stride,
				padding=1,
			),
			ConvolutionLayer(
				in_channels=mid_channels,
				out_channels=out_channels,
				kernel_size=1,
				activation=False,
			),
		]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class TransposedConvolutionBranch(Layer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, mid_channels: int, out_channels: int, stride: int):
		super().__init__()

		self.in_channels = in_channels
		self.mid_channels = mid_channels
		self.out_channels = out_channels
		self.stride = stride

		layers = [
			TransposedConvolutionLayer(
				in_channels=in_channels, out_channels=mid_channels, kernel_size=1
			),
			TransposedConvolutionLayer(
				in_channels=mid_channels,
				out_channels=mid_channels,
				kernel_size=3,
				stride=stride,
				padding=1,
				output_padding=stride - 1,
			),
			TransposedConvolutionLayer(
				in_channels=mid_channels,
				out_channels=out_channels,
				kernel_size=1,
				activation=False,
			),

		]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class ResidualConvolutionBranch(Layer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, stride: int):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.stride = stride

		if stride == 1 and in_channels == out_channels:
			layers = [Identity()]
		else:
			layers = [
				ConvolutionLayer(
					in_channels=in_channels,
					out_channels=out_channels,
					kernel_size=stride,
					stride=stride,
					activation=False,
				)
			]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class ResidualTransposedConvolutionBranch(Layer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, out_channels: int, stride: int):
		super().__init__()

		self.in_channels = in_channels
		self.out_channels = out_channels
		self.stride = stride

		if stride == 1 and in_channels == out_channels:
			layers = [Identity()]
		else:
			layers = [
				TransposedConvolutionLayer(
					in_channels=in_channels,
					out_channels=out_channels,
					kernel_size=stride,
					stride=stride,
					activation=False,
				)
			]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class ResidualLayer(nn.Module):
	# ----------------------------------------------------------------------------------------------
	def __init__(self):
		super().__init__()

	# ----------------------------------------------------------------------------------------------
	def forward(self, x: Tensor) -> Tensor:
		# s1 = x.shape
		x = self.branch_a(x) + self.branch_b(x)
		x = self.activation(x)
		# print(f"{type(self).__name__}: {list(s1[1:])} -> {list(x.shape[1:])}")
		return x


# ==================================================================================================
class ResidualConvolutionLayer(ResidualLayer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, mid_channels: int, out_channels: int, stride: int):
		super().__init__()

		self.in_channels = in_channels
		self.mid_channels = mid_channels
		self.out_channels = out_channels
		self.stride = stride

		self.branch_a = ConvolutionBranch(
			in_channels=in_channels,
			mid_channels=mid_channels,
			out_channels=out_channels,
			stride=stride,
		)
		self.branch_b = ResidualConvolutionBranch(
			in_channels=in_channels, out_channels=out_channels, stride=stride
		)
		self.activation = nn.CELU(inplace=True)


# ==================================================================================================
class ResidualTransposedConvolutionLayer(ResidualLayer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, in_channels: int, mid_channels: int, out_channels: int, stride: int):
		super().__init__()

		self.in_channels = in_channels
		self.mid_channels = mid_channels
		self.out_channels = out_channels
		self.stride = stride

		self.branch_a = TransposedConvolutionBranch(
			in_channels=in_channels,
			mid_channels=mid_channels,
			out_channels=out_channels,
			stride=stride,
		)
		self.branch_b = ResidualTransposedConvolutionBranch(
			in_channels=in_channels, out_channels=out_channels, stride=stride
		)
		self.activation = nn.CELU(inplace=True)


# ==================================================================================================
class ResidualConvolutionBlock(Layer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, num_layers: int, in_channels: int, mid_channels: int, out_channels: int):
		super(Layer, self).__init__()

		self.num_layers = num_layers
		self.in_channels = in_channels
		self.mid_channels = mid_channels
		self.out_channels = out_channels

		layers = [
			ResidualConvolutionLayer(
				in_channels=in_channels,
				mid_channels=mid_channels,
				out_channels=out_channels,
				stride=2,
			)
		] + [
			ResidualConvolutionLayer(
				in_channels=out_channels,
				mid_channels=mid_channels,
				out_channels=out_channels,
				stride=1,
			)
			for _ in range(1, num_layers)
		]

		self.layers = nn.Sequential(*layers)


# ==================================================================================================
class ResidualTransposedConvolutionBlock(Layer):
	# ----------------------------------------------------------------------------------------------
	def __init__(self, num_layers: int, in_channels: int, mid_channels: int, out_channels: int):
		super(Layer, self).__init__()

		self.num_layers = num_layers
		self.in_channels = in_channels
		self.mid_channels = mid_channels
		self.out_channels = out_channels

		layers = [
			ResidualTransposedConvolutionLayer(
				in_channels=in_channels,
				mid_channels=mid_channels,
				out_channels=out_channels,
				stride=2,
			)
		] + [
			ResidualTransposedConvolutionLayer(
				in_channels=out_channels,
				mid_channels=mid_channels,
				out_channels=out_channels,
				stride=1,
			)
			for _ in range(1, num_layers)
		]

		self.layers = nn.Sequential(*layers)
