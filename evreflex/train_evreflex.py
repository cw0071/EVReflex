#!/usr/bin/env python3
import colored_traceback.auto
import argparse
from pathlib import Path
from torchvision import transforms
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
from pytorch_lightning import loggers
import setproctitle
from kellog import info, warning, error, debug

from evreflex import utils
from evreflex.models import EVReflexNet

# ==================================================================================================
def main(args):
	setproctitle.setproctitle(args.proctitle)
	model = EVReflexNet(
		operation=args.operation,
		train_path=args.train_path,
		val_path=args.val_path,
		test_path=None,
		batch_size=args.batch_size,
		workers=args.workers,
		lr=args.lr,
		blocks=[3, 2],
		subset=args.subset,
	)
	if args.version is not None:
		import torch
		checkpointPath = args.out_dir / Path(__file__).stem / f"version_{args.version}" / "checkpoints" / "last.ckpt"
		debug(checkpointPath)
		debug(checkpointPath.exists())
	else:
		checkpointPath = None
	# `monitor` should correspond to something logged with `self.log`
	# `monitor` will apply in `validation_step()` if it's there, else `training_step()`
	checkpoint_callback = ModelCheckpoint(
		# monitor="train_loss",
		monitor="val_loss",
		filename="{epoch:02d} {train_loss:.2f}",
		save_top_k=3,
		save_last=True
	)
	if args.out_dir is None:
		warning("'out_dir' was not specified, not logging!")
		logger = None
		lr_callback = None
		callbacks = None
	else:
		logger = loggers.TensorBoardLogger(
			name=Path(__file__).stem,
			save_dir=args.out_dir,
			log_graph=True,
			version=args.version
		)
		# Default placeholder "hp_metric" neccessary so that the hyperparameters are written to file
		logger.log_hyperparams(args)
		logger.log_hyperparams(utils.get_system_info())
		lr_callback = LearningRateMonitor(logging_interval="epoch")
		callbacks = [checkpoint_callback, lr_callback]
	trainer = pl.Trainer(
		logger=logger,
		gpus=0 if args.cpu else -1,
		# resume_from_checkpoint=Path(),
		callbacks=callbacks,
		limit_train_batches=args.train_lim,
		limit_val_batches=args.val_lim,
		overfit_batches=args.overfit,
		resume_from_checkpoint=checkpointPath,
	)

	trainer.fit(model)


# ==================================================================================================
def parse_args():
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("train_path", type=Path, help="Path to EVReflex dataset, either LMDB directory or `.txt` list of relative paths")
	parser.add_argument("val_path", type=Path, help="Path to EVReflex dataset, either LMDB directory or `.txt` list of relative paths")
	parser.add_argument("operation", type=str, choices=["depth", "flow", "both", "dynamic_depth", "dynamic_both"], help="Network flavour")
	parser.add_argument("--out_dir", type=Path, help="Location to store checkpoints and logs", default=None)
	parser.add_argument("--proctitle", type=str, help="Process title", default=Path(__file__).name)
	parser.add_argument("--cpu", action="store_true", help="Run on CPU even if GPUs are available")
	parser.add_argument("-b", "--batch_size", type=int, help="Batch size", default=8)
	parser.add_argument("--lr", type=float, help="Initial learning rate", default=0.01)
	parser.add_argument("-w", "--workers", type=int, help="Number of workers for data loader", default=4)
	parser.add_argument("--train_lim", type=float, help="Proportion of train epoch length to use", default=1.0)
	parser.add_argument("--val_lim", type=float, help="Proportion of val epoch length to use", default=1.0)
	parser.add_argument("--overfit", type=float, help="Proportion of train dataset to use", default=0.0)
	parser.add_argument("--subset", type=float, help="Proportion of train dataset to use", default=None)
	parser.add_argument("-v", "--version", type=int, help="Try to continue training from this version", default=None)

	return parser.parse_args()


# ==================================================================================================
if __name__ == "__main__":
	main(parse_args())
