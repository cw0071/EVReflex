import setuptools

setuptools.setup(
	name="evreflex",
	version="0.0.1",
	author="Celyn Walters",
	author_email="celyn.walters@surrey.ac.uk",
	url="https://gitlab.eps.surrey.ac.uk/cw0071/evreflex/",
	packages=setuptools.find_packages(),
	classifiers=[
		"Programming Language :: Python :: 3",
		"Operating System :: OS Independent",
	],
	python_requires=">=3.6",
)
